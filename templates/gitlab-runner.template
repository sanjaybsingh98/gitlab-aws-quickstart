{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "AWS Cloudformation template to create an EC2 instance, installs and starts GitLab Runner. **WARNING** This template creates an Amazon EC2 instance. You will be billed for the AWS resources used if you create a stack from this template.",
    "Parameters": {
        "GitLabServer": {
            "Description": "Address of GitLab Web Server application",
            "Type": "String"
        },
        "GitLabRunnerToken": {
          "Description": "Registration token for GitLab Runner. Registration token must contain exactly 20 alphanumeric characters",
            "AllowedPattern": "^[a-zA-Z0-9]*$",
            "Type": "String",
            "MinLength": "20",
            "MaxLength": "20"
        },
        "InstanceType": {
            "Description": "The EC2 instance type for GitLab Runner server",
            "Type": "String",
            "Default": "t2.medium",
            "AllowedValues": [
                "t2.medium",
                "t2.large",
                "m3.large",
                "m3.xlarge",
                "m3.2xlarge",
                "m4.large",
                "m4.xlarge",
                "m4.2xlarge",
                "m4.4xlarge"
            ],
            "ConstraintDescription": "must be a valid EC2 instance type"
        },
        "InstanceStorageSize": {
            "Description": "GitLab Runner server storage size (in GBs)",
            "Type": "Number",
            "Default": "40"
        },
        "KeyName": {
            "Description": "Name of an existing EC2 KeyPair to enable SSH access to the instance",
            "Type": "AWS::EC2::KeyPair::KeyName",
            "ConstraintDescription": "Can contain only ASCII characters."
        },
        "VpcId": {
            "Description": "GitLab Runner server VPC",
            "Type": "AWS::EC2::VPC::Id"
        },
        "VPCCIDR": {
            "Description": "CIDR block for the existing VPC",
            "Type": "String"
        },
        "PrivateSubnet1ID": {
            "Description": "GitLab Runner server private subnet 1",
            "Type": "AWS::EC2::Subnet::Id"
        },
        "PrivateSubnet2ID": {
            "Description": "GitLab Runner server private subnet 2",
            "Type": "AWS::EC2::Subnet::Id"
        },
        "NumWebServerInstances": {
            "Description": "Number of Runner server instances in Auto scaling group",
            "Type": "Number"
        }
    },
    "Mappings": {
        "RegionMap": {
            "us-east-1": {
                "AMI": "ami-cd0f5cb6"
            },
            "us-east-2": {
                "AMI": "ami-10547475"
            },
            "us-west-1": {
                "AMI": "ami-09d2fb69"
            },
            "us-west-2": {
                "AMI": "ami-6e1a0117"
            },
            "ca-central-1": {
                "AMI": "ami-9818a7fc"
            },
            "eu-west-1": {
                "AMI": "ami-785db401"
            },
            "eu-central-1": {
                "AMI": "ami-1e339e71"
            },
            "eu-west-2": {
                "AMI": "ami-996372fd"
            },
            "ap-southeast-1": {
                "AMI": "ami-6f198a0c"
            },
            "ap-southeast-2": {
                "AMI": "ami-e2021d81"
            },
            "ap-northeast-2": {
                "AMI": "ami-d28a53bc"
            },
            "ap-northeast-1": {
                "AMI": "ami-ea4eae8c"
            },
            "ap-south-1": {
                "AMI": "ami-099fe766"
            },
            "sa-east-1": {
                "AMI": "ami-10186f7c"
            }
        }
    },
    "Resources": {
        "RunnerAutoScalingGroup": {
            "Type": "AWS::AutoScaling::AutoScalingGroup",
            "Properties": {
                "LaunchConfigurationName": {
                    "Ref": "RunnerLaunchConfiguration"
                },
                "VPCZoneIdentifier": [
                    {
                        "Ref": "PrivateSubnet1ID"
                    },
                    {
                        "Ref": "PrivateSubnet2ID"
                    }
                ],
                "MinSize": {
                    "Ref": "NumWebServerInstances"
                },
                "MaxSize": {
                    "Ref": "NumWebServerInstances"
                },
                "Cooldown": "300",
                "DesiredCapacity": {
                    "Ref": "NumWebServerInstances"
                },
                "Tags": [
                    {
                        "Key": "Name",
                        "Value": "GitLabRunner",
                        "PropagateAtLaunch": "true"
                    }
                ]
            },
            "CreationPolicy": {
                "ResourceSignal": {
                    "Count": {
                        "Ref": "NumWebServerInstances"
                    },
                    "Timeout": "PT15M"
                }
            }
        },
        "RunnerLaunchConfiguration": {
            "Type": "AWS::AutoScaling::LaunchConfiguration",
            "Metadata": {
                "Comment": "Install GitLab Runner",
                "AWS::CloudFormation::Init": {
                    "config": {
                        "packages": {
                        },
                        "files": {
                        },
                        "commands": {
                            "1-Register-Runner": {
                                "command": {
                                   "Fn::Sub": "gitlab-runner register --non-interactive --url http://${GitLabServer} --description docker --executor docker --docker-image ruby:2.4 --registration-token ${GitLabRunnerToken}"
                                }
                            },
                            "2-Start-Runner": {
                                "command": "gitlab-runner start"
                            }
                        }
                    }
                }
            },
            "Properties": {
                "KeyName": {
                    "Ref": "KeyName"
                },
                "ImageId": {
                    "Fn::FindInMap": [
                        "RegionMap",
                        {
                            "Ref": "AWS::Region"
                        },
                        "AMI"
                    ]
                },
                "SecurityGroups": [
                    {
                        "Ref": "InstanceSecurityGroup"
                    }
                ],
                "InstanceType": {
                    "Ref": "InstanceType"
                },
                "BlockDeviceMappings" : [
                    {
                    "DeviceName" : "/dev/sda1",
                    "Ebs" :
                        {
                            "VolumeSize" : {
                                "Ref": "InstanceStorageSize"
                            }
                        }
                    }
                ],
                "UserData": {
                    "Fn::Base64": {
                        "Fn::Join": [
                            "\n",
                            [
                                "#!/bin/bash",
                                "set -x -e",
                                "apt-get update",
                                "apt-get -y install apt-transport-https ca-certificates curl software-properties-common",
                                "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -",
                                "add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"",
                                "apt-get update",
                                "apt-get -y install docker-ce",
                                "curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash",
                                "apt-get -y install gitlab-runner",
                                "apt-get -y install python-setuptools",
                                "easy_install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz",
                                "counter=0",
                                {
                                    "Fn::Sub": "until $(curl --output /dev/null --silent --head --fail ${GitLabServer}); do"
                                },
                                "  echo 'waiting for GitLab...'",
                                "  counter=$(($counter+1))",
                                "  if [ $counter == 180 ]",
                                "  then",
                                "    break",
                                "  fi",
                                "  sleep 5",
                                "done",
                                {
                                    "Fn::Sub": "cfn-init --stack ${AWS::StackName} --resource RunnerLaunchConfiguration --region ${AWS::Region}"
                                },
                                {
                                    "Fn::Sub": "cfn-signal -e $? --stack ${AWS::StackName} --resource RunnerAutoScalingGroup --region ${AWS::Region}"
                                }
                            ]
                        ]
                    }
                }
            }
        },
        "InstanceSecurityGroup": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "VpcId": {
                    "Ref": "VpcId"
                },
                "GroupDescription": "Enable SSH access from Bastion via port 22",
                "SecurityGroupIngress": [
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "22",
                        "ToPort": "22",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    }
                ]
            }
        }
    }
}
